## **Phone verification**
This module provides sms verification widget for drupal core telephone field.

### Installation

Install with composer. Go to `/admin/modules` on your Drupal site 
and enable the module

### Setup

You can configure global verification settings by visiting `/admin/config/system/phone-verification`.
You can override global settings when configuring the widget settings.
