<?php

namespace Drupal\phone_verification\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Defines the PhoneVerificationLog entity.
 *
 * @ContentEntityType(
 *   id = "phone_verification_log",
 *   label = @Translation("Phone verification log"),
 *   base_table = "phone_verification_log",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 * )
 */
class PhoneVerificationLog extends ContentEntityBase {
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = [];

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Advertiser entity.'))
      ->setReadOnly(TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type'))
      ->setDescription(t('The type of entity this record belongs to'))
      ->setSettings([
        'max_length' => 64,
      ]);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Voted entity'))
      ->setDescription(t('The id of entity this record belongs to'));

    $fields['field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Field name'))
      ->setDescription(t('The field name this record belongs to'))
      ->setSettings([
        'max_length' => FieldStorageConfig::NAME_MAX_LENGTH,
      ]);

    $fields['telephone_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Telephone number'))
      ->setDescription(t('Telephone number for verification'));

    $fields['sent_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Sent code'))
      ->setDescription(t('Verification code sent via SMS'));

    $fields['sent_at'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Sent at'))
      ->setDescription(t('Datetime of form submit'));

    $fields['valid_until'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Valid until'))
      ->setDescription(t('Datetime of code devaluation'));

    $fields['verified'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Verified'))
      ->setDescription(t('Defines if entry was verified'));

    $fields['verified_at'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Verified at'))
      ->setDescription(t('Datetime of submitting the verification code'));

    return $fields;
  }
}
