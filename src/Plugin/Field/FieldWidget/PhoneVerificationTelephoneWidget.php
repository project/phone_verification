<?php

namespace Drupal\phone_verification\Plugin\Field\FieldWidget;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\phone_verification\PhoneVerificationInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\telephone\Plugin\Field\FieldWidget\TelephoneDefaultWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'phone_verification_telephone' widget.
 *
 * @FieldWidget(
 *   id = "phone_verification_telephone",
 *   label = @Translation("Phone verification telephone number"),
 *   field_types = {
 *     "telephone"
 *   }
 * )
 */
class PhoneVerificationTelephoneWidget extends TelephoneDefaultWidget implements ContainerFactoryPluginInterface {

  protected $config;

  protected $phone_verification;

  protected $module_handler;

  protected $entity_type_id;

  protected $entity_id;

  protected $field_name;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, PhoneVerificationInterface $phone_verification, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->config = \Drupal::config('phone_verification.settings');
    $this->phone_verification = $phone_verification;
    $this->module_handler = $module_handler;
    $this->entity_type_id = $this->fieldDefinition->getTargetEntityTypeId();
    $this->entity_id = NULL;
    $this->field_name = $this->fieldDefinition->getName();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('phone_verification'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings =  parent::defaultSettings();

    $settings['code_type'] = NULL;
    $settings['code_length'] = NULL;
    $settings['sms_message'] = NULL;

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary =  parent::settingsSummary();

    $code_length = $this->getSetting('code_length');
    if ($code_length) {
      $summary[] = $this->t('Verification code length: @length',['@length' => $code_length]);
    }

    $code_type = $this->getSetting('code_type');
    if (isset($code_types[$code_type])) {
      $code_types = $this->phone_verification->getCodeTypes();
      $summary[] = $this->t('Verification code type: @type',['@type' => $code_types[$code_type]]);
    }

    $sms_message = $this->getSetting('sms_message');
    if ($sms_message !== NULL) {
      $summary[] = $this->t('SMS Message: @sms',['@sms' => $sms_message]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['notice'] = [
      '#markup' => $this->t('Override global module settings here. If you leave a setting empty, global value will be used.')
    ];

    $element['code_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Verification code length'),
      '#default_value' => $this->getSetting('code_length'),
    ];

    $element['code_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Verification code type'),
      '#default_value' => $this->getSetting('code_type'),
      '#empty_option' => $this->t('Use default'),
      '#options' => $this->phone_verification->getCodeTypes(),
    ];

    $element['sms_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SMS Message'),
      '#value' => $this->getSetting('sms_message'),
      '#description' => $this->t('SMS Message must contain @code placeholder for generated code. You can use core tokens in this field.
        To use advanced tokens and token browser, install token module.'),
      '#default_value' => $this->t('@code'),
      '#element_validate' => [[$this,'smsMessageFieldValidator']],
    ];

    if ($this->module_handler->moduleExists('token')) {
      $element['token_browser'] = [
        '#theme' => 'token_tree_link',
        '#click_insert' => FALSE,
        '#dialog' => TRUE,
      ];
    }

    return $element;
  }

  /**
   * Validation function for message field.
   */
  public function smsMessageFieldValidator($element, FormStateInterface $form_state) {
    $value = $form_state->getValue($element['#parents']);

    // Check if SMS message contains @code placeholder.
    if ($value && strpos($value,'@code') === FALSE) {
      $form_state->setError($element,$this->t('SMS Message must contain @code placeholder for generated code'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    /** @var \Drupal\Core\Entity\ContentEntityFormInterface $form_object */
    $form_object = $form_state->getFormObject();
    $entity_id = $form_object->getEntity()->id();
    if ($entity_id) {
      $this->entity_id = $entity_id;
    }

    $element['#attached']['library'][] = 'phone_verification/widget';

    $element['#element_validate'][] = [$this, 'elementValidate'];

    $element['#process'][] = [$this, 'process'];

    $field_name = $this->fieldDefinition->getName();
    $wrapper_id = str_replace('_', '-', $field_name) . '-' . $delta . '-phone-verification-wrapper';

    $element['value']['#weight'] = -1;

    $value = $form_state->getValue(array_merge($element['#field_parents'], [$field_name, $delta]));
    $default_number = $element['value']['#default_value'];

    $number = NULL;
    if (!empty($value['value'])) {
      $number = $value['value'];
    }
    elseif ($default_number) {
      $number = $default_number;
    }

    $verified = $this->phone_verification->isVerifiedNumber($this->entity_type_id, $this->entity_id, $this->field_name, $number, $default_number);
    $show_verify = !empty($value['show_verify']);

    $wrapper_classes = ['phone-verification-wrapper'];
    // Add state classes for easier theming
    if ($verified) {
      $wrapper_classes[] = 'state-verified';
    }
    elseif ($show_verify) {
      $wrapper_classes[] = 'state-code-sent';
    }
    else {
      $wrapper_classes[] = 'state-initial';
    }

    $element['#prefix'] = '<div id="' . $wrapper_id . '" class="' . implode(' ', $wrapper_classes) . '">';
    $element['#suffix'] = '</div>';

    $element['send_sms'] = [
      '#type' => 'submit',
      '#validate' => [[$this,'sendSMSValidator']],
      '#submit' => [[$this,'sendSMSSubmit']],
      '#value' => $this->t('Send verification SMS'),
      '#ajax' => [
        'callback' => [$this,'updateElementCallback'],
        'wrapper' => $wrapper_id
      ],
      '#attributes' => [
        'class' => ['send-sms'],
      ],
    ];

    if ($verified) {
      $element['send_sms']['#attributes']['style'] = 'display: none;';
      $element['message'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->t('Number verified!'),
        '#attributes' => [
          'class' => ['verified-message'],
        ],
      ];
    }
    elseif ($show_verify) {
      $element['code_sent_message'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->t('A verification code has been sent to your telephone. Please enter it here to verify your phone number.'),
        '#attributes' => [
          'class' => ['code-sent-message'],
        ],
      ];
      $element['sms_code'] = [
        '#type' => 'textfield',
        '#title' => $this->t('SMS verification code'),
        '#required' => TRUE,
        '#wrapper_attributes' => [
          'class' => ['sms-verification-code'],
        ],
      ];
      $element['verify_number'] = [
        '#type' => 'submit',
        '#value' => $this->t('Verify'),
        '#submit' => [[$this,'verifySMSSubmit']],
        '#validate' => [[$this,'verifySMSValidator']],
        '#ajax' => [
          'callback' => [$this,'updateElementCallback'],
          'wrapper' => $wrapper_id,
        ],
        '#attributes' => [
          'class' => ['verify-number'],
        ],
      ];
    }

    return $element;
  }

  /**
   * Process callback.
   *
   * Limit validation errors for action buttons.
   */
  public function process($element, FormStateInterface $form_state, &$form) {
    $element['send_sms']['#limit_validation_errors'] = [array_merge($element['#parents'], ['value'])];
    $element['verify_number']['#limit_validation_errors'] = [$element['#parents']];
    $element['send_sms']['#name'] = implode('__', $element['#parents']) . '__send_sms';
    $element['verify_number']['#name'] = implode('__', $element['#parents']) . '__verify_number';
    if (!$this->entity_id) {
      $form['actions']['submit']['#submit'][] = [$this, 'updateLogRecordsSubmit'];
    }
    return $element;
  }

  public function updateLogRecordsSubmit(&$form, FormStateInterface $form_state) {
    $storage = $form_state->getStorage();
    if (!empty($storage['phone_verification_log_ids'])) {
      /** @var \Drupal\Core\Entity\ContentEntityFormInterface $form_object */
      $form_object = $form_state->getFormObject();
      $this->entity_id = $form_object->getEntity()->id();
      $this->phone_verification->updateLogRecordsEntityId($this->entity_id, $storage['phone_verification_log_ids']);
    }
  }

  public function sendSMSValidator(array &$form, FormStateInterface $form_state) {
    $parent_element = $this->getParentElement($form, $form_state);
    $parent_value = $this->getParentElementValue($form_state);
    $number = $parent_value['value'];
    if (empty($number)) {
      $form_state->setError($parent_element['value'], $this->t('No telephone number entered'));
    }
  }

  /**
   * Submit function for send sms button.
   *
   * Sets helper values for the element.
   */
  public function sendSMSSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();

    $parent_value = $this->getParentElementValue($form_state);
    $parent_element = $this->getParentElement($form, $form_state);

    $default_number = $parent_element['value']['#default_value'];
    $number = $parent_value['value'];

    if ($this->phone_verification->isVerifiedNumber($this->entity_type_id, $this->entity_id, $this->field_name, $number, $default_number)) {
      return;
    }

    // Generate code, get phone number and send SMS
    $code = $this->phone_verification->generateCode($this->getSetting('code_length'), $this->getSetting('code_type'));

    $message = $this->getSetting('sms_message');
    if (empty($message)) {
      $message = $this->config->get('sms_message');
    }

    $result = $this->phone_verification->sendVerificationCode($this->entity_type_id, $this->entity_id, $this->field_name, $number, $code, $message);

    $show_verify = FALSE;
    if (empty($result['message_result']) || $result['message_result']->getError()) {
      $this->messenger()->addError(
        $this->t('There was a problem sending an SMS, check entered phone number')
      );
    }
    else {
      // Save created record id to form storage, so entity id can be updated
      // after form submit
      $storage = $form_state->getStorage();
      $storage['phone_verification_log_ids'][] = $result['log_id'];
      $form_state->setStorage($storage);
      $show_verify = TRUE;
    }
    $form_state->setValue(array_merge($parent_element['#parents'], ['show_verify']), $show_verify);
  }

  /**
   * Validation function for verify button.
   *
   * Verifies the validity of entered code.
   */
  public function verifySMSValidator(&$form, FormStateInterface &$form_state) {
    $parent_value = $this->getParentElementValue($form_state);

    $code = $parent_value['sms_code'];
    $number = $parent_value['value'];

    $parent_element = $this->getParentElement($form, $form_state);

    $code_valid = $this->phone_verification->verifyCode($this->entity_type_id, $this->entity_id, $this->field_name, $number, $code);
    if (!$code_valid) {
      $form_state->setError($parent_element['sms_code'],
        t('Incorrect code or entry not valid.'));
      $form_state->setRebuild();
    }
  }

  /**
   * Submit function for verify button.
   *
   * Sets helper values for the element.
   */
  public function verifySMSSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax update callback for the element.
   */
  public function updateElementCallback(array &$form, FormStateInterface $form_state) {
    // Rebuild phone number verification form part
    return $this->getParentElement($form, $form_state);
  }

  /**
   * Validate the element on form submit
   */
  public function elementValidate($element, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $field_name = $this->fieldDefinition->getName();
    $value = $form_state->getValue($element['#parents']);

    $number = $value['value'];
    $default_number = $element['value']['#default_value'];

    // Do not set errors when validation is triggered by this element or its children
    // Only trigger this error when submitting the form
    if ($number && !$this->phone_verification->isVerifiedNumber($this->entity_type_id, $this->entity_id, $this->field_name, $number, $default_number) && !in_array($field_name, $triggering_element['#array_parents'])) {
      $form_state->setError($element['value'], $this->t('Telephone number not validated'));
    }
  }

  /**
   * Helper function to get value of the main parent element
   *
   * @return array
   */
  public function getParentElementValue(FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $triggering_element = $form_state->getTriggeringElement();
    $field_name = $this->fieldDefinition->getName();
    $field_name_index = array_search($field_name, $triggering_element['#parents']);
    return NestedArray::getValue($values, array_slice($triggering_element['#parents'], 0, $field_name_index + 2));
  }

  /**
   * Helper function to get the main parent element
   *
   * @return array
   */
  public function getParentElement(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $field_name = $this->fieldDefinition->getName();
    $field_name_index = array_search($field_name, $triggering_element['#array_parents']);
    return NestedArray::getValue($form, array_slice($triggering_element['#array_parents'], 0, $field_name_index + 3));
  }

}
