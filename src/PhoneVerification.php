<?php

namespace Drupal\phone_verification;

use Drupal\phone_verification\Entity\PhoneVerificationLog;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\sms\Exception\RecipientRouteException;
use Drupal\sms\Message\SmsMessage;
use Drupal\sms\Provider\SmsProviderInterface;

class PhoneVerification implements PhoneVerificationInterface {

  protected $module_config;

  protected $sms_provider;

  public function __construct(ConfigFactoryInterface $config_factory, SmsProviderInterface $sms_provider) {
    $this->module_config = $config_factory->get('phone_verification.settings');
    $this->sms_provider = $sms_provider;
  }

  /**
   * {@inheritdoc}
   */
  public function getCodeTypes() {
    return [
      0 => t('Only numbers'),
      1 => t('Only letters'),
      2 => t('Numbers and letters'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isVerifiedNumber($entity_type_id, $entity_id, $field_name, $telephone_number, $default_number = NULL) {
    if (empty($telephone_number)) {
      return FALSE;
    }
    if (!empty($_SESSION['phone_verification_verified_numbers']) && in_array($telephone_number, $_SESSION['phone_verification_verified_numbers'])) {
      return TRUE;
    }
    $entity_query = \Drupal::entityQuery('phone_verification_log')
      ->accessCheck(FALSE)
      ->condition('verified',TRUE)
      ->condition('entity_type', $entity_type_id)
      ->condition('field_name', $field_name)
      ->condition('telephone_number', $telephone_number);
    // If default number is set and equals current number, we do not check
    // the code validity timestamp
    if ($telephone_number != $default_number) {
      $entity_query->condition('valid_until', time(), '>');
    }
    if ($entity_id) {
      $entity_query->condition('entity_id', $entity_id);
    }
    else {
      $entity_query->notExists('entity_id');
    }
    $ids = $entity_query->execute();

    if (!empty($ids)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function generateCode($code_length = NULL, $code_type = NULL) {
    if (!$code_length) {
      $code_length = $this->module_config->get('code_length');
    }
    if ($code_type === NULL) {
      $code_type = $this->module_config->get('code_type');
    }

    $code = '';

    switch ($code_type) {
      case 0: {
        // Only numbers
        for ($i = 0; $i < $code_length; $i++) {
          $code .= rand(0,9);
        }
        break;
      }
      case 1: {
        // Only letters
        $code = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $code_length);
        break;
      }
      case 2: {
        // Numbers and letters
        $code = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, $code_length);
        break;
      }
    }

    return $code;
  }

  /**
   * {@inheritdoc}
   */
  public function sendVerificationCode($entity_type_id, $entity_id, $field_name, $telephone_number, $code, $message_text = '') {
    if (empty($message_text)) {
      $message_text = $this->module_config->get('sms_message');
    }
    $message_text = str_replace('@code', $code, $message_text);
    $message_text = \Drupal::token()->replace($message_text);

    try {
      $sms_message = new SmsMessage(NULL, [$telephone_number], $message_text);

      /** @var SmsMessage $message */
      $message = $this->sms_provider->send($sms_message)[0];
      $log_id = NULL;

      if (!$message->getResult()->getError()) {
        // Create a log record
        $now = strtotime('now');
        $log = PhoneVerificationLog::create([
          'entity_type' => $entity_type_id,
          'entity_id' => $entity_id,
          'field_name' => $field_name,
          'telephone_number' => $telephone_number,
          'sent_code' => $code,
          'sent_at' => $now,
          'valid_until' => $now + $this->module_config->get('code_expiration'),
          'verified' => FALSE,
        ]);
        $log->save();
        $log_id = $log->id();
      }

      return [
        'message_result' => $message->getResult(),
        'log_id' => $log_id,
      ];
    }
    catch (RecipientRouteException $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function verifyCode($entity_type_id, $entity_id, $field_name, $telephone_number, $code) {
    // Get logs matching phone number and code
    $entity_query = \Drupal::entityQuery('phone_verification_log')
      ->accessCheck(FALSE)
      ->condition('entity_type', $entity_type_id)
      ->condition('field_name', $field_name)
      ->condition('telephone_number', $telephone_number)
      ->condition('sent_code', $code)
      ->condition('verified', FALSE)
      ->condition('valid_until', strtotime('now'), '>');
    if ($entity_id) {
      $entity_query->condition('entity_id', $entity_id);
    }
    else {
      $entity_query->notExists('entity_id');
    }
    $log_ids = $entity_query->execute();

    if (!empty($log_ids)) {
      // Reset entityQuery array, because we need only one value
      reset($log_ids);
      $log_id = key($log_ids);

      $log = PhoneVerificationLog::load($log_id);

      $log->set('verified',TRUE);
      $log->set('verified_at', strtotime('now'));

      $log->save();

      // Store verified numbers in session, so user does not have to verify
      // the same number twice
      $_SESSION['phone_verification_verified_numbers'][] = $telephone_number;
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateLogRecordsEntityId($entity_id, array $record_ids) {
    $records = PhoneVerificationLog::loadMultiple($record_ids);
    foreach ($records as $record) {
      $record->set('entity_id', $entity_id);
      $record->save();
    }
  }

}
