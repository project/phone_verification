<?php

namespace Drupal\phone_verification\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\phone_verification\PhoneVerificationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PhoneVerificationSettingsForm extends ConfigFormBase {

  const SETTINGS = 'phone_verification.settings';

  protected $phone_verification;

  protected $module_handler;

  public function __construct(ConfigFactoryInterface $config_factory, PhoneVerificationInterface $phone_verification, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->phone_verification = $phone_verification;
    $this->module_handler = $module_handler;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('phone_verification'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'phone_verification_admin_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['code_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Verification code length'),
      '#default_value' => $config->get('code_length'),
      '#required' => TRUE
    ];

    $form['code_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Verification code type'),
      '#default_value' => $config->get('code_type'),
      '#options' => $this->phone_verification->getCodeTypes(),
      '#required' => TRUE
    ];

    // Show code expiration in hours, for better user experience
    $code_expiration = $config->get('code_expiration');

    $form['code_expiration'] = [
      '#type' => 'number',
      '#title' => $this->t('Code expiration time (seconds)'),
      '#default_value' => $code_expiration,
      '#required' => TRUE
    ];

    $form['sms_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SMS Message'),
      '#description' => $this->t('SMS Message must contain @code placeholder for generated code. You can use core tokens in this field.
        To use advanced tokens and token browser, install token module.'),
      '#default_value' => $config->get('sms_message'),
      '#element_validate' => [[$this,'smsMessageFieldValidator']],
      '#required' => TRUE,
    ];

    if ($this->module_handler->moduleExists('token')) {
      $form['token_browser'] = [
        '#theme' => 'token_tree_link',
        '#click_insert' => FALSE,
        '#dialog' => TRUE,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  public function smsMessageFieldValidator($element, FormStateInterface $form_state) {
    $value = $form_state->getValue($element['#parents']);

    // Check if SMS message contains @code placeholder.
    if (strpos($value,'@code') === FALSE) {
      $form_state->setError($element,$this->t('SMS Message must contain @code placeholder for generated code'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('code_length', $form_state->getValue('code_length'))
      ->set('code_type', $form_state->getValue('code_type'))
      ->set('code_expiration', $form_state->getValue('code_expiration'))
      ->set('sms_message',$form_state->getValue('sms_message'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
