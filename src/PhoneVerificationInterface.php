<?php

namespace Drupal\phone_verification;

interface PhoneVerificationInterface {

  /**
   * Returns available code types.
   *
   * @return array
   *   An array of code type names keyed by code type key
   */
  public function getCodeTypes();

  /**
   * Helper function to check if number is verified
   *
   * This process makes sure that user won't need to verify
   * phone number multiple times during period of its validity
   *
   * @param string $entity_type_id
   *   The id of the entity type the verified number belongs to.
   * @param int $entity_id
   *   The id of the entity the verified number belongs to.
   * @param string $field_name
   *   The name of the field the verified number belongs to.
   * @param string $telephone_number
   *   The number to check.
   * @param string $default_number
   *   The default value for the telephone number field.
   * @return bool
   *   TRUE if the number is verified, FALSE otherwise
   */
  public function isVerifiedNumber($entity_type_id, $entity_id, $field_name, $telephone_number, $default_number = NULL);

  /**
   * Generates a verification code.
   *
   * @param int $code_length
   * @param int $code_type
   *
   * @return string
   */
  public function generateCode($code_length = NULL, $code_type = NULL);

  /**
   * @param string $entity_type_id
   * @param int $entity_id
   * @param string $field_name
   * @param string $telephone_number
   * @param string $code
   * @param string $message_text
   *
   * @return array
   *   An associative array containing SMS message result and created log record id.
   */
  public function sendVerificationCode($entity_type_id, $entity_id, $field_name, $telephone_number, $code, $message_text = '');

  /**
   * Verifies if the entered code is valid.
   *
   * @param string $entity_type_id
   * @param int $entity_id
   * @param string $field_name
   * @param $telephone_number
   * @param $code
   *
   * @return bool
   *   TRUE if verification was successful, FALSE otherwise
   */
  public function verifyCode($entity_type_id, $entity_id, $field_name, $telephone_number, $code);

  /**
   * Update entity_id on phone_verification_log entities.
   *
   * @param int $entity_id
   * @param array $record_ids
   */
  public function updateLogRecordsEntityId($entity_id, array $record_ids);
}
