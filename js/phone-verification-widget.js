(function ($, Drupal, drupalSettings) {
  
  'use strict';
  
  Drupal.behaviors.phoneVerificationWidget = {
    attach: function (context) {
      $(".phone-verification-wrapper").each(function () {
        let wrapper = $(this);
        let phoneVal = wrapper.find('.form-tel').val();
        if (wrapper.find('input.error').length) {
          wrapper.find('.send-sms').show();
          wrapper.find('.verified-message').remove();
        }
        wrapper.on('keyup', '.form-tel', function () {
          if (phoneVal !== $(this).val()) {
            wrapper.find('.send-sms').show();
            wrapper.find('.verified-message').remove();
            if (wrapper.hasClass('state-verified')) {
              wrapper.removeClass('state-verified').addClass('state-initial');
            }
          }
        });
      });
    }
  };
  
})(jQuery, Drupal, drupalSettings);
